package appsnmobilesolutions.com.lovtov.activity;

import android.net.Uri;

/**
 * Created by dev on 11/15/17.
 */

interface OnFragmentInteractionListener {
    public void onFragmentInteraction(Uri uri);
}
