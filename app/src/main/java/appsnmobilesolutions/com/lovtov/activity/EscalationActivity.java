package appsnmobilesolutions.com.lovtov.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.adapter.EscalationAdapter;
import appsnmobilesolutions.com.lovtov.model.Escalation;
import appsnmobilesolutions.com.lovtov.util.ConnectionDetector;
import dmax.dialog.SpotsDialog;

import static appsnmobilesolutions.com.lovtov.util.APILinks.ESCALATION_URL;
import static appsnmobilesolutions.com.lovtov.util.APILinks.SEND_ESCALATION_URL;

public class EscalationActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    FloatingActionButton fabAddComment;

    Bitmap thumbnail;

    ImageButton btnUpload;
    EditText complaint;

    private static final int CAPTURE_PHOTO = 123;
    final int REQUEST_CODE_GALLERY = 999;

    String imagePath = "";

    Uri mImageUri = null;

    Dialog dialog;

    private static List<Escalation> escalationList;
    EscalationAdapter escalationAdapter;


    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    private SpotsDialog progressDialog;

    ConnectionDetector connectionDetector;
    RecyclerView recyclerEscalation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escalation);

        registerComponents();
    }

    public void registerComponents() {
        toolbar = findViewById(R.id.toolbar);
        fabAddComment = findViewById(R.id.fabAddComment);
        recyclerEscalation = findViewById(R.id.recyclerEscalation);

        progressDialog = new SpotsDialog(this, R.style.Custom);
        progressDialog.setCancelable(true);
        connectionDetector = new ConnectionDetector(this);
        escalationList = new ArrayList<>();
        prefs = PreferenceManager.getDefaultSharedPreferences(EscalationActivity.this);
        edit = prefs.edit();

        escalationAdapter = new EscalationAdapter(escalationList, EscalationActivity.this);

        toolbar.setTitle("Escalations");
        toolbar.setTitleTextColor(getResources().getColor(R.color.text_color));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        fabAddComment.setOnClickListener(this);

        recyclerEscalation.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(EscalationActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerEscalation.setLayoutManager(llm);

        recyclerEscalation.setAdapter(escalationAdapter);

        displayingEscalation();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAddComment:
                complaintDialog();
                //Toast.makeText(this, "Add Comment Button", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    ImageView imgComplaint;

    private void complaintDialog() {
        dialog = new Dialog(EscalationActivity.this);
        dialog.setContentView(R.layout.new_escalation);
        dialog.setTitle("Update");

        imgComplaint = dialog.findViewById(R.id.imageComplaint);
        btnUpload = dialog.findViewById(R.id.imgbtnUpload);
        complaint = dialog.findViewById(R.id.edtComplaint);
        Button btnSend = dialog.findViewById(R.id.btnSend);

        btnUpload.setOnClickListener(view -> uploadingImage());

        btnSend.setOnClickListener(view -> {
            if (complaint.getText().toString().isEmpty() && mImageUri == null) {

                Toast.makeText(EscalationActivity.this, "All fields are required", Toast.LENGTH_SHORT).show();
            } else if (!complaint.getText().toString().isEmpty()) {
                progressDialog.show();
                sendingEscalation();
            }
        });
        dialog.show();
    }

    private void sendingEscalation() {
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {

            String escalations = complaint.getText().toString();
            File imageFile = new File(prefs.getString("image_string", ""));
            Ion.with(this)
                    .load("POST", SEND_ESCALATION_URL)
                    .setLogging("Escalation Logs", Log.DEBUG)
                    .setMultipartParameter("engaged_code", prefs.getString("engaged_code", "N/A"))
                    .setMultipartParameter("complaint_desc", escalations)
                    .setMultipartFile("file", imageFile)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                String jsonString = result.toString();
                                JSONObject jsonObject = new JSONObject(jsonString);
                                Log.d("Results", jsonString);

                                if (jsonObject.getString("resp_code").equals("013")) {

                                    Toast.makeText(this, "" + jsonObject.getString("resp_desc"), Toast.LENGTH_SHORT).show();
                                    complaint.setText("");
                                    edit.putString("image_string","");
                                    progressDialog.dismiss();
                                    dialog.dismiss();


                                    displayingEscalation();
                                    //this.finish();
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(this, "Escalation sending failed", Toast.LENGTH_SHORT).show();
                                    //displayDialog(getResources().getString(R.string.invalid_username_password));
                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                                //progressDialog.dismiss();
                                //displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            //progressDialog.dismiss();
                            //displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            //progressDialog.dismiss();
            //displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }

    }


    private void displayingEscalation() {
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {

            JsonObject loginObjects = new JsonObject();
            loginObjects.addProperty("engaged_code", prefs.getString("engaged_code", "N/A"));
            escalationList.clear();


            Ion.with(this)
                    .load("POST", ESCALATION_URL)
                    .setLogging("Esca Logs", Log.DEBUG)
                    .setJsonObjectBody(loginObjects)
                    .asJsonArray()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                Escalation escalation;
                                String jsonString = result.toString();
                                Log.d("Results", jsonString);

                                JSONArray escalationArrayJSON = new JSONArray(jsonString);


                                for (int i = 0; i < escalationArrayJSON.length(); i++) {
                                    Random rnd = new Random();
                                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

                                    JSONObject escalationObject = escalationArrayJSON.getJSONObject(i);
                                    String approverId = escalationObject.getString("approver_id");
                                    String escalationDesc = escalationObject.getString("complaint_desc");
                                    String approvalStatus = escalationObject.getString("approval_status");
                                    String updatedAt = escalationObject.getString("date");
                                    String image_path = escalationObject.getString("image_path");

                                    escalation = new Escalation();
                                    escalation.setApprover_id("Approved by: Jude");
                                    escalation.setEscalation(escalationDesc);
                                    escalation.setApproval_status(approvalStatus);
                                    escalation.setUpdated_at(updatedAt);
                                    escalation.setImage_path(image_path);
                                    escalation.setSide_color(color);

                                    escalationList.add(escalation);

                                }
                                escalationAdapter.notifyDataSetChanged();


                                //progressDialog.dismiss();
                                //finishActivity();

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                //progressDialog.dismiss();
                                //displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            //progressDialog.dismiss();
                            //displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            //progressDialog.dismiss();
            //displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadingImage() {
        CharSequence[] items = {"Take a photo", "Use existing photo"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(EscalationActivity.this);

        dialog.setTitle("Choose an action");
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, CAPTURE_PHOTO);
                        break;

                    case 1:
                        Intent intent1 = new Intent(Intent.ACTION_PICK);
                        intent1.setType("image/*");
                        startActivityForResult(intent1, REQUEST_CODE_GALLERY);
                        break;
                }
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_PHOTO) {
            if (resultCode == RESULT_OK) {

                onCaptureImageResult(data);
            }
        }

        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {

            mImageUri = data.getData();

            String[] filePath = {MediaStore.Images.Media.DATA};

            Cursor c = EscalationActivity.this.getContentResolver().query(mImageUri, filePath, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);

            if (!imagePath.isEmpty() && new File(imagePath).exists()) {
                new File(imagePath).delete();
            }
            imagePath = c.getString(columnIndex);
            c.close();

            //set Image from gallery
            File image_file = new File(imagePath);
            if (image_file.exists()) {
                Picasso.with(this).load(new File(imagePath)).into(imgComplaint);
                edit.putString("image_string", imagePath);
                edit.commit();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        mImageUri = data.getData();
        String[] filePath = {MediaStore.Images.Media.DATA};

        Cursor c = EscalationActivity.this.getContentResolver().query(mImageUri, filePath, null, null, null);
        c.moveToFirst();
        int columnIndex = c.getColumnIndex(filePath[0]);

        if (!imagePath.isEmpty() && new File(imagePath).exists()) {
            new File(imagePath).delete();
        }
        imagePath = c.getString(columnIndex);
        c.close();


        //set Image from Camera
        File image_file = new File(imagePath);
        if (image_file.exists()) {
            Picasso.with(this).load(new File(imagePath)).into(imgComplaint);
            edit.putString("image_string", imagePath);
            edit.commit();
        }
    }
}
