package appsnmobilesolutions.com.lovtov.activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import appsnmobilesolutions.com.lovtov.R;

public class MainAppActivity extends AppCompatActivity implements View.OnClickListener {
    TextView employeeName, employeeCode, phoneNumber;
    ImageView userAvatar;
    Typeface typeface;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    ImageView imgProfile, imgHealth, imgComplaint, imgBioData, imgSchedule, imgNotications;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ContextCompat.checkSelfPermission(MainAppActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainAppActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
        setContentView(R.layout.activity_main_app);
        registerComponents();
    }


    public void registerComponents() {
        prefs = PreferenceManager.getDefaultSharedPreferences(MainAppActivity.this);
        edit = prefs.edit();
        typeface = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        employeeName = findViewById(R.id.employee_name);
        employeeCode = findViewById(R.id.employee_code);
        phoneNumber = findViewById(R.id.employee_number);
        userAvatar = findViewById(R.id.user_avatar);
        employeeName.setText("Welcome " + prefs.getString("employee_first_name", "N/A"));
        employeeCode.setText(prefs.getString("engaged_code", "N/A"));
        employeeName.setTypeface(typeface);
        employeeCode.setTypeface(typeface);
        phoneNumber.setTypeface(typeface);

        imgBioData = findViewById(R.id.bio_data);
        imgComplaint = findViewById(R.id.complaint);
        imgHealth = findViewById(R.id.health_info);
        imgNotications = findViewById(R.id.notifications);
        imgSchedule = findViewById(R.id.schedule);
        imgProfile = findViewById(R.id.my_profile);

        imgBioData.setOnClickListener(this);
        imgComplaint.setOnClickListener(this);
        imgHealth.setOnClickListener(this);
        imgNotications.setOnClickListener(this);
        imgSchedule.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.my_profile:
                break;

            case R.id.health_info:
                startActivity(new Intent(MainAppActivity.this, HealthInfoActivity.class));
                break;

            case R.id.notifications:
                break;

            case R.id.schedule:
                startActivity(new Intent(MainAppActivity.this, MedScheduleActivity.class));
                break;

            case R.id.complaint:
                startActivity(new Intent(MainAppActivity.this, EscalationActivity.class));
                break;

            case R.id.bio_data:
                startActivity(new Intent(MainAppActivity.this, BioData.class));
                break;
        }
    }
}
