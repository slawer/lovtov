package appsnmobilesolutions.com.lovtov.activity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import org.json.JSONObject;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.util.ConnectionDetector;
import dmax.dialog.SpotsDialog;

import static appsnmobilesolutions.com.lovtov.util.APILinks.BIO_DATA_URL;
import static appsnmobilesolutions.com.lovtov.util.APILinks.POST_BIO_DATA_URL;

public class BioData extends AppCompatActivity {
    Typeface typeface;
    CardView cardView;
    EditText first_name, last_name, other_names, age, dob, emp_code, contact_number, contact_email, alt_number, postal_address, residence_address, location, marital_status;
    Button bio_data_update_button;
    ConnectionDetector connectionDetector;
    private SpotsDialog progressDialog;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    Toolbar toolbar;

    String marital_status_id="";
    String _person_id="";
    String residence_city_id="";
    String gender="";
    String record_type_id="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_data);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Bio Data");
        toolbar.setTitleTextColor(getResources().getColor(R.color.text_color));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        typeface = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        cardView = findViewById(R.id.card_view);
        progressDialog = new SpotsDialog(this);

        first_name = findViewById(R.id.first_name);
        last_name = cardView.findViewById(R.id.last_name);
        other_names = cardView.findViewById(R.id.other_names);
        age = cardView.findViewById(R.id.age);
        dob = cardView.findViewById(R.id.dob);
        emp_code = cardView.findViewById(R.id.employee_code);
        contact_number = cardView.findViewById(R.id.contact_number);
        contact_email = cardView.findViewById(R.id.contact_email);
        alt_number = cardView.findViewById(R.id.alternative_number);
        postal_address = cardView.findViewById(R.id.postal_address);
        residence_address = cardView.findViewById(R.id.residence_address);
        location = cardView.findViewById(R.id.location);
        marital_status = cardView.findViewById(R.id.marital_status);


        bio_data_update_button = findViewById(R.id.bio_data_update_button);

        //companyNameEditText.setTypeface(typeface);
        first_name.setTypeface(typeface);
        last_name.setTypeface(typeface);
        other_names.setTypeface(typeface);
        age.setTypeface(typeface);
        dob.setTypeface(typeface);
        emp_code.setTypeface(typeface);
        contact_number.setTypeface(typeface);
        contact_email.setTypeface(typeface);
        alt_number.setTypeface(typeface);
        postal_address.setTypeface(typeface);
        residence_address.setTypeface(typeface);
        location.setTypeface(typeface);
        marital_status.setTypeface(typeface);

        bio_data_update_button.setTypeface(typeface);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        edit = prefs.edit();

        bio_data_update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmationDialog();
            }
        });

        getBioData();
    }

    public void getBioData() {
        connectionDetector = new ConnectionDetector(BioData.this);
        if (connectionDetector.isNetworkAvailable()) {

            JsonObject loginObjects = new JsonObject();
            loginObjects.addProperty("engaged_code", prefs.getString("engaged_code", "N/A"));

            Ion.with(this)
                    .load("POST", BIO_DATA_URL)
                    .setLogging("BioData Logs", Log.DEBUG)
                    .setJsonObjectBody(loginObjects)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                String jsonString = result.toString();
                                Log.d("BioData JSON", jsonString);

                                JSONObject jsonObject = new JSONObject(jsonString);
                                String f_name = jsonObject.getString("first_name");
                                String l_name = jsonObject.getString("last_name");
                                String o_names = jsonObject.getString("other_names");
                                String dob_ = jsonObject.getString("dob");
                                String age_ = jsonObject.getString("age");
                                String employee_code = jsonObject.getString("employee_code");
                                String con_number = jsonObject.getString("contact_number");
                                String con_email = jsonObject.getString("contact_email");
                                String alternative_number = jsonObject.getString("alternative_number");
                                String post_address = jsonObject.getString("postal_address");
                                String res_address = jsonObject.getString("residence_address");
                                String loc = jsonObject.getString("location");
                                String marital = jsonObject.getString("marital_status");
                                marital_status_id = jsonObject.getString("marital_status_id");
                                _person_id = jsonObject.getString("person_id");
                                residence_city_id = jsonObject.getString("residence_city_id");
                                gender = jsonObject.getString("gender");
                                record_type_id = jsonObject.getString("record_type_id");

                                first_name.setText(f_name);
                                last_name.setText(l_name);
                                other_names.setText(o_names);
                                dob.setText(dob_);
                                age.setText(age_);
                                emp_code.setText(employee_code);
                                contact_number.setText(con_number);
                                contact_email.setText(con_email);
                                alt_number.setText(alternative_number);
                                postal_address.setText(post_address);
                                residence_address.setText(res_address);
                                location.setText(loc);
                                marital_status.setText(marital);

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                progressDialog.dismiss();
                                //hideKeyBoard();
                                Toast.makeText(BioData.this, "Oops something happened. Please try again later", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(BioData.this, "Oops something happened. Please try again later", Toast.LENGTH_SHORT).show();
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            //progressDialog.dismiss();
            //displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    private void showConfirmationDialog() {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        else
            builder = new android.app.AlertDialog.Builder(this);

        builder.setMessage("Are you sure?")
                .setPositiveButton("Yes", (dialog, which) -> {
                            progressDialog.show();
                            postBioData();

                        }
                )
                .setNegativeButton("No", (dialog, which) -> {
                    dialog.dismiss();
                });

        builder.setCancelable(false);
        builder.show();

    }

    private void showDialog(String message) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        else
            builder = new android.app.AlertDialog.Builder(this);

        builder.setMessage(message)
                .setPositiveButton("Ok", (dialog, which) -> {
                            finish();

                        }
                );

        builder.setCancelable(false);
        builder.show();

    }

    private void postBioData() {
        connectionDetector = new ConnectionDetector(BioData.this);
        if (connectionDetector.isNetworkAvailable()) {

            JsonObject loginObjects = new JsonObject();
            loginObjects.addProperty("engaged_code", prefs.getString("engaged_code", "N/A"));
            loginObjects.addProperty("first_name", first_name.getText().toString());
            loginObjects.addProperty("last_name", last_name.getText().toString());
            loginObjects.addProperty("other_names", other_names.getText().toString());
            loginObjects.addProperty("dob", dob.getText().toString());
            loginObjects.addProperty("age", age.getText().toString());
            loginObjects.addProperty("contact_number", contact_number.getText().toString());
            loginObjects.addProperty("contact_email", contact_email.getText().toString());
            loginObjects.addProperty("alternative_number", alt_number.getText().toString());
            loginObjects.addProperty("postal_address", postal_address.getText().toString());
            loginObjects.addProperty("residence_address", residence_address.getText().toString());
            loginObjects.addProperty("location", location.getText().toString());
            loginObjects.addProperty("marital_status", marital_status.getText().toString());
            loginObjects.addProperty("marital_status_id", marital_status_id);
            loginObjects.addProperty("person_id", _person_id);
            loginObjects.addProperty("residence_city_id", residence_city_id);
            loginObjects.addProperty("gender", gender);
            loginObjects.addProperty("record_type_id", record_type_id);



            Ion.with(this)
                    .load("POST", POST_BIO_DATA_URL)
                    .setLogging("Post BioData Logs", Log.DEBUG)
                    .setJsonObjectBody(loginObjects)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                String jsonString = result.toString();
                                Log.d("BioData JSON", jsonString);

                                JSONObject jsonObject = new JSONObject(jsonString);
                                if(jsonObject.getString("resp_code").equals("000")){
                                    showDialog(jsonObject.getString("resp_desc"));
                                }
                                else if(jsonObject.getString("resp_code").equals("011")){
                                    showDialog(jsonObject.getString("resp_desc"));
                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                                progressDialog.dismiss();
                                //hideKeyBoard();
                                Toast.makeText(BioData.this, "Oops something happened. Please try again later", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(BioData.this, "Oops something happened. Please try again later", Toast.LENGTH_SHORT).show();
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            //progressDialog.dismiss();
            //displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

}
