package appsnmobilesolutions.com.lovtov.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.util.ConnectionDetector;
import appsnmobilesolutions.com.lovtov.util.CustomViewDialog;
import dmax.dialog.SpotsDialog;

import static appsnmobilesolutions.com.lovtov.util.APILinks.LOGIN_URL;
import static appsnmobilesolutions.com.lovtov.util.APILinks.USER_TYPE_URL;

public class SignInActivity extends AppCompatActivity {

    TextView signInTV;
    Typeface typeface;
    CardView cardView;
    EditText usernameEditText, passwordEditText;
    Button signInButton;
    TextView signInTextView;
    LinearLayout signUpLinear;
    ConnectionDetector connectionDetector;
    private SpotsDialog progressDialog;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    Spinner spinnerUserType;

    String usertype_id = "";
    String usertype_desc = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }


        typeface = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        cardView = findViewById(R.id.card_view);
        signInTV = findViewById(R.id.sign_in_tv);
        usernameEditText = cardView.findViewById(R.id.username);
        passwordEditText = cardView.findViewById(R.id.password);
        signInButton = findViewById(R.id.sign_in_button);
        signInTextView = findViewById(R.id.new_account_tv);
        signUpLinear = findViewById(R.id.sign_up_linear);
        signInTV.setTypeface(typeface);
        usernameEditText.setTypeface(typeface);
        passwordEditText.setTypeface(typeface);
        signInButton.setTypeface(typeface);
        signInTextView.setTypeface(typeface);
        progressDialog = new SpotsDialog(this, R.style.Custom);
        progressDialog.setCancelable(true);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        edit = prefs.edit();

        spinnerUserType = cardView.findViewById(R.id.spinnerUserType);

        //TextView textView =  progressDialog.findViewById(android.R.id.message);
        //textView.setTypeface(typeface);

        getUserTypes();

        signInButton.setOnClickListener(v -> {
            if (validateFieldBounds()) {
                progressDialog.show();
                doSignIn();
            } else {
                displayDialog(getResources().getString(R.string.provide_details));
            }
        });


        signUpLinear.setOnClickListener(v -> {
            startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
            //finish();
        });

    }

    public void getUserTypes() {
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {

            Ion.with(this)
                    .load("GET", USER_TYPE_URL)
                    .setLogging("Usertype Logs", Log.DEBUG)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                HashMap<Integer, String> spinnerMap = new HashMap<>();

                                String jsonString = result.toString();
                                JSONObject jsonObject = new JSONObject(jsonString);
                                Log.d("Results", jsonString);
                                switch (jsonObject.getString("resp_code")) {
                                    case "000":
                                        //JSONObject resultJSON = new JSONObject(jsonObject.getString("results"));
                                        JSONArray resultJSON = new JSONArray(jsonObject.getString("results"));
                                        String[] spinnerArray = new String[resultJSON.length()];

                                        for (int i = 0; i < resultJSON.length(); i++) {

                                            JSONObject allergyObject = resultJSON.getJSONObject(i);

                                            String id = allergyObject.getString("id").trim();
                                            String user_type = allergyObject.getString("user_type_desc").trim();
                                            String user_type_short = allergyObject.getString("user_type_short").trim();

                                            spinnerMap.put(i, id);
                                            spinnerArray[i] = user_type;


                                            Log.d("loggedRe", id + " " + user_type + " " + user_type_short);
                                        }

                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinnerUserType.setAdapter(adapter);

                                        //Get value to spinner
                                        spinnerUserType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                                usertype_desc = spinnerUserType.getSelectedItem().toString();
                                                usertype_id = spinnerMap.get(spinnerUserType.getSelectedItemPosition());

                                                //doSomethingWith(cardList.get(key));
                                                //toggleVisibility(usertype_id, usertype_desc);
                                            }


                                            @Override
                                            public void onNothingSelected(AdapterView<?> adapterView) {

                                            }
                                        });

                                        progressDialog.dismiss();
                                        break;
                                    case "008":
                                        progressDialog.dismiss();
                                        displayDialog("User with credentials exist");
                                        break;
                                    case "010":
                                        progressDialog.dismiss();
                                        displayDialog("You cannot access this service");
                                        break;
                                    default:
                                        progressDialog.dismiss();
                                        displayDialog(getResources().getString(R.string.unknown_error));
                                        break;
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                progressDialog.dismiss();
                                displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            progressDialog.dismiss();
                            displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            progressDialog.dismiss();
            displayDialog(getResources().getString(R.string.network_unavailable));
        }
    }


    public boolean validateFieldBounds() {
        if (TextUtils.isEmpty(usernameEditText.getText().toString())) {
            usernameEditText.setError(getResources().getString(R.string.user_name_error));
            return false;
        }


        if (TextUtils.isEmpty(passwordEditText.getText().toString())) {
            passwordEditText.setError(getResources().getString(R.string.password_error));
            return false;
        }

        return true;
    }


    public void doSignIn() {
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {

            JsonObject loginObjects = new JsonObject();
            loginObjects.addProperty("username", usernameEditText.getText().toString());
            loginObjects.addProperty("user_type", usertype_id);
            loginObjects.addProperty("password", passwordEditText.getText().toString());

            Ion.with(this)
                    .load("POST", LOGIN_URL)
                    .setLogging("Login Logs", Log.DEBUG)
                    .setJsonObjectBody(loginObjects)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                String jsonString = result.toString();
                                JSONObject jsonObject = new JSONObject(jsonString);


                                Log.d("Results", jsonString);

                                if (jsonObject.getString("resp_code").equals("000")) {
                                    edit.putString("employee_first_name", jsonObject.getString("employee_first_name"));
                                    edit.putString("employee_last_name", jsonObject.getString("employee_last_name"));
                                    edit.putString("engaged_code", jsonObject.getString("engaged_code"));
                                    edit.putString("age", jsonObject.getString("age"));
                                    edit.putString("user_type_id", jsonObject.getString("user_type_id"));
                                    edit.commit();
                                    progressDialog.dismiss();
                                    finishActivity();
                                }
                                else if (jsonObject.getString("resp_code").equals("011")) {
                                    edit.putString("offsite_first_name", jsonObject.getString("offsite_first_name"));
                                    edit.putString("offsite_last_name", jsonObject.getString("offsite_last_name"));
                                    edit.putString("offsite_email", jsonObject.getString("offsite_email"));
                                    edit.putString("user_type_id", jsonObject.getString("user_type_id"));
                                    edit.commit();
                                    progressDialog.dismiss();
                                    finishOffsiteActivity();
                                }
                                else {
                                    progressDialog.dismiss();
                                    displayDialog(getResources().getString(R.string.invalid_username_password));
                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                                progressDialog.dismiss();
                                displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            progressDialog.dismiss();
                            displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            progressDialog.dismiss();
            displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }


    public void finishActivity() {
        startActivity(new Intent(SignInActivity.this, MainAppActivity.class));
        finish();
    }

    public void finishOffsiteActivity() {
        startActivity(new Intent(SignInActivity.this, JobVacancyActivity.class));
        finish();
    }

    public void displayDialog(String message) {
        CustomViewDialog alert = new CustomViewDialog();
        alert.showDialogTwo(SignInActivity.this, message);
    }
}
