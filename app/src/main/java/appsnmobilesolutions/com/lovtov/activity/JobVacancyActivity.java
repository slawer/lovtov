package appsnmobilesolutions.com.lovtov.activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import appsnmobilesolutions.com.lovtov.adapter.AllergyAdapter;
import appsnmobilesolutions.com.lovtov.adapter.HealthConditionAdapter;
import appsnmobilesolutions.com.lovtov.model.Allergy;
import appsnmobilesolutions.com.lovtov.model.HealthCondition;
import appsnmobilesolutions.com.lovtov.model.JobParent;
import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.adapter.JobVacancyAdapter;
import appsnmobilesolutions.com.lovtov.model.JobVacancyInfo;
import appsnmobilesolutions.com.lovtov.util.ConnectionDetector;
import dmax.dialog.SpotsDialog;

import static appsnmobilesolutions.com.lovtov.util.APILinks.JOB_VACANCY_URL;
import static appsnmobilesolutions.com.lovtov.util.APILinks.MED_INFO_URL;

public class JobVacancyActivity extends AppCompatActivity {

    private List<JobParent> jobParentList;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    private SpotsDialog progressDialog;
    JobVacancyAdapter jobVacancyAdapter;
    RecyclerView jobRecyclerView;
    ConnectionDetector connectionDetector;
    TextView healthConditionHeader, allergyHeader;
    Typeface typeface;
    Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_vacancy);

        registerComponents();
    }

    private void registerComponents() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Job Vacancies");
        toolbar.setTitleTextColor(getResources().getColor(R.color.text_color));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());

        connectionDetector = new ConnectionDetector(this);
        prefs = PreferenceManager.getDefaultSharedPreferences(JobVacancyActivity.this);
        edit = prefs.edit();


        jobParentList=new ArrayList<>();

        // jobVacancyAdapter=new JobVacancyAdapter(jobParentList);

        typeface = Typeface.createFromAsset(this.getAssets(), "Lato-Regular.ttf");
        jobRecyclerView = findViewById(R.id.recyclerJobVacancy);

        getJobVacancy();

        jobRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(JobVacancyActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        jobRecyclerView.setLayoutManager(llm);
        //jobRecyclerView.setAdapter(jobVacancyAdapter);
    }

    public void getJobVacancy() {
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {

//            JsonObject loginObjects = new JsonObject();
//            loginObjects.addProperty("engaged_code", prefs.getString("engaged_code", "N/A"));

            Ion.with(this)
                    .load("GET", JOB_VACANCY_URL)
                    .setLogging("Job Logs", Log.DEBUG)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {



                                String jsonString = result.toString();
                                JSONObject jsonObject = new JSONObject(jsonString);
                                Log.d("Results", jsonString);

                                if (jsonObject.getString("resp_code").equals("000")) {
                                    JSONArray resultJSON = new JSONArray(jsonObject.getString("results"));

                                    for (int i = 0; i < resultJSON.length(); i++) {
                                        Random rnd = new Random();
                                        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

                                        JSONObject allergyObject = resultJSON.getJSONObject(i);
                                        String userAllergy = allergyObject.getString("job_title");
                                        String recordDate = allergyObject.getString("job_desc");

                                        ArrayList<JobVacancyInfo> jobVacancyInfo = new ArrayList<>();
                                        jobVacancyInfo.add(new JobVacancyInfo(recordDate,color));
                                        //jobVacancyInfo.setDateRecorded("Recorded On: " + recordDate);
                                        jobParentList.add(new JobParent(userAllergy,jobVacancyInfo));
                                    }



                                    jobVacancyAdapter = new JobVacancyAdapter(jobParentList);

                                    jobRecyclerView.setAdapter(jobVacancyAdapter);

                                    //progressDialog.dismiss();
                                    //finishActivity();
                                } else {
                                    //progressDialog.dismiss();
                                    //displayDialog(getResources().getString(R.string.invalid_username_password));
                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                                //progressDialog.dismiss();
                                //displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            //progressDialog.dismiss();
                            //displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            //progressDialog.dismiss();
            //displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                this.finish();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
}
