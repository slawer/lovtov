package appsnmobilesolutions.com.lovtov.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import appsnmobilesolutions.com.lovtov.R;

public class HomeActivity extends AppCompatActivity {
    TextView lovTov, description;
    Typeface typeface;
    Button signUp, signIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        lovTov = findViewById(R.id.lov_top_text);
        description = findViewById(R.id.lov_top_description);
        typeface = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        lovTov.setTypeface(typeface);
        description.setTypeface(typeface);

        signUp = findViewById(R.id.sign_up);
        signIn = findViewById(R.id.sign_in);
        signUp.setTypeface(typeface);
        signIn.setTypeface(typeface);



        signIn.setOnClickListener(v -> startActivity(new Intent(HomeActivity.this,SignInActivity.class)));


        signUp.setOnClickListener(v -> startActivity(new Intent(HomeActivity.this,JobVacancyActivity.class)));

    }
}
