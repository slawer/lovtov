package appsnmobilesolutions.com.lovtov.activity;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.adapter.AllergyAdapter;
import appsnmobilesolutions.com.lovtov.adapter.HealthConditionAdapter;
import appsnmobilesolutions.com.lovtov.model.Allergy;
import appsnmobilesolutions.com.lovtov.model.HealthCondition;
import appsnmobilesolutions.com.lovtov.util.ConnectionDetector;
import dmax.dialog.SpotsDialog;

import static appsnmobilesolutions.com.lovtov.util.APILinks.MED_INFO_URL;

public class MedicalInformation extends AppCompatActivity {

    private static List<Allergy> allergylist;
    private static List<HealthCondition> healthConditionList;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    private SpotsDialog progressDialog;
    AllergyAdapter allergyAdapter;
    HealthConditionAdapter healthConditionAdapter;
    RecyclerView recyclerView, healthRecyclerView;
    ConnectionDetector connectionDetector;
    TextView healthConditionHeader, allergyHeader;
    Typeface typeface;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_information);
        connectionDetector = new ConnectionDetector(this);
        allergylist = new ArrayList<>();
        healthConditionList = new ArrayList<>();
        prefs = PreferenceManager.getDefaultSharedPreferences(MedicalInformation.this);
        edit = prefs.edit();




        typeface = Typeface.createFromAsset(this.getAssets(), "Lato-Regular.ttf");
        recyclerView = findViewById(R.id.allergies);
        healthRecyclerView = findViewById(R.id.health_conditions);
        recyclerView.setHasFixedSize(true);
        healthRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(MedicalInformation.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);



        //LinearLayoutManager llm2 = new LinearLayoutManager(MedicalInformation.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        //healthRecyclerView.setLayoutManager(llm);



        healthConditionHeader = findViewById(R.id.health_header);
        allergyHeader = findViewById(R.id.allergy_header);
        allergyHeader.setTypeface(typeface);
        healthConditionHeader.setTypeface(typeface);

        getMedicalInformation();
        setUpToolBar();

    }


    public void getMedicalInformation() {
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {

            JsonObject loginObjects = new JsonObject();
            loginObjects.addProperty("engaged_code", prefs.getString("engaged_code", "N/A"));

            Ion.with(this)
                    .load("POST", MED_INFO_URL)
                    .setLogging("Med Logs", Log.DEBUG)
                    .setJsonObjectBody(loginObjects)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                Allergy allergy;
                                HealthCondition healthCondition;
                                String jsonString = result.toString();
                                JSONObject jsonObject = new JSONObject(jsonString);
                                Log.d("Results", jsonString);

                                if (jsonObject.getString("resp_code").equals("000")) {
                                    JSONArray allergyArrayJSON = new JSONArray(jsonObject.getString("allergies"));
                                    JSONArray conditionArrayJSON = new JSONArray(jsonObject.getString("health_conditions"));

                                    for (int i = 0; i < allergyArrayJSON.length(); i++) {
                                        JSONObject allergyObject = allergyArrayJSON.getJSONObject(i);
                                        String userAllergy = allergyObject.getString("allergy");
                                        String recordDate = allergyObject.getString("record_date");
                                        allergy = new Allergy();
                                        allergy.setAllergyName("Allergy: " + userAllergy);
                                        allergy.setDateRecorded("Recorded On: " + recordDate);
                                        allergylist.add(allergy);
                                    }


                                    for (int i = 0; i < conditionArrayJSON.length(); i++) {
                                        JSONObject healthObject = conditionArrayJSON.getJSONObject(i);
                                        String healthConditionName = healthObject.getString("condition");
                                        String dateRecorded = healthObject.getString("record_date");
                                        healthCondition = new HealthCondition();
                                        healthCondition.setConditionName("Condition: " + healthConditionName);
                                        healthCondition.setDateRecorded("Recorded On" + dateRecorded);
                                        healthConditionList.add(healthCondition);
                                    }


                                    healthConditionAdapter = new HealthConditionAdapter(healthConditionList, getApplicationContext());
                                    allergyAdapter = new AllergyAdapter(allergylist, getApplicationContext());

                                    recyclerView.setAdapter(allergyAdapter);
                                    healthRecyclerView.setAdapter(healthConditionAdapter);


                                    //progressDialog.dismiss();
                                    //finishActivity();
                                } else {
                                    //progressDialog.dismiss();
                                    //displayDialog(getResources().getString(R.string.invalid_username_password));
                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                                //progressDialog.dismiss();
                                //displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            //progressDialog.dismiss();
                            //displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            //progressDialog.dismiss();
            //displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void setUpToolBar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowTitleEnabled(true);
                actionBar.setTitle("Medical Information");
                actionBar.setDisplayUseLogoEnabled(false);
                actionBar.setHomeButtonEnabled(true);
            }
        }
    }
}
