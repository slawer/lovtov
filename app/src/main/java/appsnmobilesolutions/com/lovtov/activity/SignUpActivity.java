package appsnmobilesolutions.com.lovtov.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.util.ConnectionDetector;
import appsnmobilesolutions.com.lovtov.util.CustomViewDialog;
import dmax.dialog.SpotsDialog;

import static appsnmobilesolutions.com.lovtov.util.APILinks.OFFSITE_SIGN_UP_URL;
import static appsnmobilesolutions.com.lovtov.util.APILinks.SIGN_UP_URL;
import static appsnmobilesolutions.com.lovtov.util.APILinks.USER_TYPE_URL;

public class SignUpActivity extends AppCompatActivity {
    TextView signUpTV;
    Typeface typeface;
    CardView cardView;
    EditText confrimEditText, IDEdittext, usernameEditText, passwordEditText;

    TextInputLayout confrimTLT, IDTLT, usernameTLT, passwordTLT;
    TextInputLayout firstnameTLT, lastnameTLT, othernamesTLT, emailTLT, phoneTLT, dobTLT;

    TextView genderTLT;
    RadioGroup radgroupGender;

    Spinner spinnerUserType;

    RadioButton maleRadioButton, femaleRadioButton;

    EditText firstnameEditText, lastnameEdittext, othernamesEditText, emailEditText, phoneEditText, dobEditText, genderEditText;
    Button signUpButton;
    TextView signInTextView;
    LinearLayout signInLinear;
    ConnectionDetector connectionDetector;
    private SpotsDialog progressDialog;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    String usertype_id = "";
    String usertype_desc = "";
    String gender = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        getUserTypes();
        typeface = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        cardView = findViewById(R.id.card_view);
        progressDialog = new SpotsDialog(this);
        signUpTV = findViewById(R.id.sign_up_tv);
        signUpTV.setTypeface(typeface);

        confrimTLT = findViewById(R.id.tltConPassword);
        IDTLT = findViewById(R.id.tltUniqueId);
        usernameTLT = findViewById(R.id.tltUsername);
        passwordTLT = findViewById(R.id.tltPassword);
        firstnameTLT = findViewById(R.id.tltFirstname);
        lastnameTLT = findViewById(R.id.tltLastname);
        othernamesTLT = findViewById(R.id.tltOthername);
        emailTLT = findViewById(R.id.tltEmail);
        phoneTLT = findViewById(R.id.tltPhoneNumber);
        dobTLT = findViewById(R.id.tltDob);
        genderTLT = findViewById(R.id.tltGender);
        radgroupGender = findViewById(R.id.radgroupGender);

        confrimEditText = findViewById(R.id.confirm_password);
        IDEdittext = cardView.findViewById(R.id.unique_id);
        usernameEditText = cardView.findViewById(R.id.username);
        passwordEditText = cardView.findViewById(R.id.password);
        firstnameEditText = cardView.findViewById(R.id.first_name);
        lastnameEdittext = cardView.findViewById(R.id.surname);
        othernamesEditText = cardView.findViewById(R.id.other_names);
        phoneEditText = cardView.findViewById(R.id.phone_number);
        emailEditText = cardView.findViewById(R.id.email);
        dobEditText = cardView.findViewById(R.id.dob);
        dobEditText = cardView.findViewById(R.id.dob);
        maleRadioButton = cardView.findViewById(R.id.radMale);
        femaleRadioButton = cardView.findViewById(R.id.radFemale);
        spinnerUserType = cardView.findViewById(R.id.spinnerUserType);

        signUpButton = findViewById(R.id.sign_up_button);
        signInTextView = findViewById(R.id.sign_in_textview);
        signInLinear = findViewById(R.id.sign_in_linear);
        //companyNameEditText.setTypeface(typeface);
        IDEdittext.setTypeface(typeface);
        usernameEditText.setTypeface(typeface);
        passwordEditText.setTypeface(typeface);
        confrimEditText.setTypeface(typeface);
        signUpButton.setTypeface(typeface);
        signInTextView.setTypeface(typeface);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        edit = prefs.edit();
        progressDialog = new SpotsDialog(this, R.style.Custom);
        progressDialog.setCancelable(true);

        signInLinear.setOnClickListener(v -> {
            startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
            finish();
        });


        dobEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(SignUpActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        Log.d("DEBUG", "year: " + year + " month: " + month + " day: " + day);
                        dobEditText.setText(new StringBuffer().append(year).append("-").append(month).append("-").append(day));
                    }
                },mYear, mMonth, mDay);
                mDatePicker.show();


            }
        });


        maleRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender = maleRadioButton.getText().toString();
            }
        });

        femaleRadioButton.setOnClickListener(v -> {
            gender = femaleRadioButton.getText().toString();
        });

        signUpButton.setOnClickListener(v -> processAccount());


    }



    public boolean validateFieldBounds() {
        if (IDEdittext.getText().toString().length() < 0) {
            IDEdittext.setError("Please Enter A Valid Employee Code");
            return false;
        }

        if (usernameEditText.getText().toString().length() < 0) {
            usernameEditText.setError("Please Enter A Valid Username");
            return false;
        }


        if (passwordEditText.getText().toString().length() < 0) {
            passwordEditText.setError("Please Enter A Password");
            return false;
        }

        if (!passwordEditText.getText().toString().equals(confrimEditText.getText().toString())) {
            passwordEditText.setError("Passwords Do Not Match");
            confrimEditText.setError("Passwords Do Not Match");
            return false;
        }

        return true;
    }

    public boolean validateFieldBoundsOff() {
        if (firstnameEditText.getText().toString().length() < 0) {
            firstnameEditText.setError("Please Enter A Valid Firstname");
            return false;
        }

        if (lastnameEdittext.getText().toString().length() < 0) {
            lastnameEdittext.setError("Please Enter A Valid Lastname");
            return false;
        }


        if (othernamesEditText.getText().toString().length() < 0) {
            othernamesEditText.setError("Please Enter A Othername(s)");
            return false;
        }

        if (!passwordEditText.getText().toString().equals(confrimEditText.getText().toString())) {
            passwordEditText.setError("Passwords Do Not Match");
            confrimEditText.setError("Passwords Do Not Match");
            return false;
        }

        if (emailEditText.getText().toString().length() < 0 || !emailEditText.getText().toString().isEmpty()) {
//            passwordEditText.setError("Passwords Do Not Match");
//            confrimEditText.setError("Passwords Do Not Match");
            boolean em=true;
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches()){
                emailEditText.setError("Please Enter Correct Email");
                em=false;
            }

            return em;
        }

        if (phoneEditText.getText().toString().length() < 0) {
            phoneEditText.setError("Please Enter A Phone Number");
            return false;
        }

        if (dobEditText.getText().toString().length() < 0) {
            dobEditText.setError("Please Enter A Date of Birth)");
            return false;
        }

        return true;
    }

    public void createNewAccount() {
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {


                JsonObject signUpObjects = new JsonObject();
                signUpObjects.addProperty("unique_id", IDEdittext.getText().toString());
                signUpObjects.addProperty("username", usernameEditText.getText().toString());
                signUpObjects.addProperty("password", passwordEditText.getText().toString());
                signUpObjects.addProperty("user_type_id", usertype_id);

                Ion.with(this)
                        .load("POST", SIGN_UP_URL)
                        .setLogging("Sign Up Logs", Log.DEBUG)
                        .setJsonObjectBody(signUpObjects)
                        .asJsonObject()
                        .setCallback((e, result) -> {

                            if (result != null) {
                                try {
                                    String jsonString = result.toString();
                                    JSONObject jsonObject = new JSONObject(jsonString);
                                    Log.d("Results", jsonString);
                                    if (jsonObject.getString("resp_code").equals("000")) {
                                        //JSONObject resultJSON = new JSONObject(jsonObject.getString("results"));
                                        // edit.putString("employee_first_name", resultJSON.getString("first_name"));
                                        // edit.putString("employee_last_name", resultJSON.getString("last_name"));
                                        // edit.putString("engaged_code", resultJSON.getString("engaged_code"));
                                        // Log.d("F Name", resultJSON.getString("first_name") + "");
                                        // Log.d("L Name", resultJSON.getString("last_name") + "");
                                        // Log.d("Code", resultJSON.getString("engaged_code") + "");
                                        edit.commit();
                                        progressDialog.dismiss();

                                        startActivity(new Intent(this, SignInActivity.class));
                                        finish();
                                    } else if (jsonObject.getString("resp_code").equals("008")) {
                                        progressDialog.dismiss();
                                        displayDialog("User with credentials exist");
                                    } else if (jsonObject.getString("resp_code").equals("010")) {
                                        progressDialog.dismiss();
                                        displayDialog("You cannot access this service");
                                    } else {
                                        progressDialog.dismiss();
                                        displayDialog(getResources().getString(R.string.unknown_error));
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    progressDialog.dismiss();
                                    displayDialog(getResources().getString(R.string.unknown_error));
                                }
                            } else {
                                progressDialog.dismiss();
                                displayDialog(getResources().getString(R.string.unknown_error));
                                Log.e("Ion Exception", "wtf", e);
                            }
                        });

        } else {

            progressDialog.dismiss();
            displayDialog(getResources().getString(R.string.network_unavailable));
        }
    }

    public void getUserTypes() {
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {


            Ion.with(this)
                    .load("GET", USER_TYPE_URL)
                    .setLogging("Usertype Logs", Log.DEBUG)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                HashMap<Integer, String> spinnerMap = new HashMap<>();

                                String jsonString = result.toString();
                                JSONObject jsonObject = new JSONObject(jsonString);
                                Log.d("Results", jsonString);
                                switch (jsonObject.getString("resp_code")) {
                                    case "000":
                                        //JSONObject resultJSON = new JSONObject(jsonObject.getString("results"));
                                        JSONArray resultJSON = new JSONArray(jsonObject.getString("results"));
                                        String[] spinnerArray = new String[resultJSON.length()];

                                        for (int i = 0; i < resultJSON.length(); i++) {

                                            JSONObject allergyObject = resultJSON.getJSONObject(i);

                                            String id = allergyObject.getString("id").trim();
                                            String user_type = allergyObject.getString("user_type_desc").trim();
                                            String user_type_short = allergyObject.getString("user_type_short").trim();

                                            spinnerMap.put(i, id);
                                            spinnerArray[i] = user_type;


                                            Log.d("loggedRe", id + " " + user_type + " " + user_type_short);
                                        }

                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinnerUserType.setAdapter(adapter);

                                        //Get value to spinner
                                        spinnerUserType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                                usertype_desc = spinnerUserType.getSelectedItem().toString();
                                                usertype_id = spinnerMap.get(spinnerUserType.getSelectedItemPosition());

                                                //doSomethingWith(cardList.get(key));
                                                toggleVisibility(usertype_id, usertype_desc);
                                            }


                                            @Override
                                            public void onNothingSelected(AdapterView<?> adapterView) {

                                            }
                                        });

                                        progressDialog.dismiss();
                                        break;
                                    case "008":
                                        progressDialog.dismiss();
                                        displayDialog("User with credentials exist");
                                        break;
                                    case "010":
                                        progressDialog.dismiss();
                                        displayDialog("You cannot access this service");
                                        break;
                                    default:
                                        progressDialog.dismiss();
                                        displayDialog(getResources().getString(R.string.unknown_error));
                                        break;
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                progressDialog.dismiss();
                                displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            progressDialog.dismiss();
                            displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            progressDialog.dismiss();
            displayDialog(getResources().getString(R.string.network_unavailable));
        }
    }

    public void toggleVisibility(String usertype_id, String usertype_desc) {

        if (usertype_desc.equals(getString(R.string.employee))) {
            confrimTLT.setVisibility(View.VISIBLE);
            IDTLT.setVisibility(View.VISIBLE);
            usernameTLT.setVisibility(View.VISIBLE);
            passwordTLT.setVisibility(View.VISIBLE);
            firstnameTLT.setVisibility(View.GONE);
            lastnameTLT.setVisibility(View.GONE);
            othernamesTLT.setVisibility(View.GONE);
            emailTLT.setVisibility(View.GONE);
            phoneTLT.setVisibility(View.GONE);
            dobTLT.setVisibility(View.GONE);
            genderTLT.setVisibility(View.GONE);
            radgroupGender.setVisibility(View.GONE);
        } else if (usertype_desc.equals(getString(R.string.offsite_user))) {
            confrimTLT.setVisibility(View.VISIBLE);
            IDTLT.setVisibility(View.GONE);
            usernameTLT.setVisibility(View.GONE);
            passwordTLT.setVisibility(View.VISIBLE);
            firstnameTLT.setVisibility(View.VISIBLE);
            lastnameTLT.setVisibility(View.VISIBLE);
            othernamesTLT.setVisibility(View.VISIBLE);
            emailTLT.setVisibility(View.VISIBLE);
            phoneTLT.setVisibility(View.VISIBLE);
            dobTLT.setVisibility(View.VISIBLE);
            genderTLT.setVisibility(View.VISIBLE);
            radgroupGender.setVisibility(View.VISIBLE);
        }
    }

    public void createNewOffsiteAccount() {

        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {

                JsonObject signUpObjects = new JsonObject();
                signUpObjects.addProperty("first_name", firstnameEditText.getText().toString());
                signUpObjects.addProperty("last_name", lastnameEdittext.getText().toString());
                signUpObjects.addProperty("other_name", othernamesEditText.getText().toString());
                signUpObjects.addProperty("password", passwordEditText.getText().toString());
                signUpObjects.addProperty("email", emailEditText.getText().toString());
                signUpObjects.addProperty("phone_number", phoneEditText.getText().toString());
                signUpObjects.addProperty("dob", dobEditText.getText().toString());
                signUpObjects.addProperty("user_type_id", usertype_id);
                signUpObjects.addProperty("gender", gender);

                Ion.with(this)
                        .load("POST", OFFSITE_SIGN_UP_URL)
                        .setLogging("Sign Up Logs", Log.DEBUG)
                        .setJsonObjectBody(signUpObjects)
                        .asJsonObject()
                        .setCallback((e, result) -> {

                            if (result != null) {
                                try {
                                    String jsonString = result.toString();
                                    JSONObject jsonObject = new JSONObject(jsonString);
                                    Log.d("Results", jsonString);
                                    switch (jsonObject.getString("resp_code")) {
                                        case "000":
                                            //JSONObject resultJSON = new JSONObject(jsonObject.getString("results"));
                                            // edit.putString("employee_first_name", resultJSON.getString("first_name"));
                                            // edit.putString("employee_last_name", resultJSON.getString("last_name"));
                                            // edit.putString("engaged_code", resultJSON.getString("engaged_code"));
                                            // Log.d("F Name", resultJSON.getString("first_name") + "");
                                            // Log.d("L Name", resultJSON.getString("last_name") + "");
                                            // Log.d("Code", resultJSON.getString("engaged_code") + "");
                                            //edit.commit();
                                            progressDialog.dismiss();

                                            startActivity(new Intent(this, SignInActivity.class));
                                            finish();
                                            break;
                                        case "008":
                                            progressDialog.dismiss();
                                            displayDialog("User with credentials exist");
                                            break;
                                        case "010":
                                            progressDialog.dismiss();
                                            displayDialog("You cannot access this service");
                                            break;
                                        default:
                                            progressDialog.dismiss();
                                            displayDialog(getResources().getString(R.string.unknown_error));
                                            break;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    progressDialog.dismiss();
                                    displayDialog(getResources().getString(R.string.unknown_error));
                                }
                            } else {
                                progressDialog.dismiss();
                                displayDialog(getResources().getString(R.string.unknown_error));
                                Log.e("Ion Exception", "wtf", e);
                            }
                        });

        } else {

            progressDialog.dismiss();
            displayDialog(getResources().getString(R.string.network_unavailable));
        }
    }

    public void displayDialog(String message) {
        CustomViewDialog alert = new CustomViewDialog();
        alert.showDialogTwo(SignUpActivity.this, message);
    }

    private void processAccount(){
        if (usertype_desc.equals(getString(R.string.employee))) {
            if(validateFieldBounds()) {
                progressDialog.show();
                createNewAccount();
            }
        }
        else if(usertype_desc.equals(getString(R.string.offsite_user))) {
            if (validateFieldBoundsOff()) {
                progressDialog.show();
                createNewOffsiteAccount();
            }
        }
    }
}
