package appsnmobilesolutions.com.lovtov.activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.adapter.AllergyAdapter;
import appsnmobilesolutions.com.lovtov.adapter.MedScheduleAdapter;
import appsnmobilesolutions.com.lovtov.model.Allergy;
import appsnmobilesolutions.com.lovtov.model.MedSchedule;
import appsnmobilesolutions.com.lovtov.util.ConnectionDetector;

import static appsnmobilesolutions.com.lovtov.util.APILinks.BIO_DATA_URL;
import static appsnmobilesolutions.com.lovtov.util.APILinks.MED_SCHEDULE_URL;

public class MedScheduleActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerSchedule;

    private static List<MedSchedule> medScheduleList;

    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    ConnectionDetector connectionDetector;

    MedScheduleAdapter medScheduleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_med_schedule);

        toolbar = findViewById(R.id.toolbar);
        recyclerSchedule = findViewById(R.id.recyclerSchedule);

        connectionDetector = new ConnectionDetector(this);
        medScheduleList = new ArrayList<>();

        toolbar.setTitle("Schedules");
        toolbar.setTitleTextColor(getResources().getColor(R.color.text_color));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        edit = prefs.edit();
        recyclerSchedule.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerSchedule.setLayoutManager(llm);

        getSchedule();
    }

    public void getSchedule() {
        connectionDetector = new ConnectionDetector(MedScheduleActivity.this);
        if (connectionDetector.isNetworkAvailable()) {

            JsonObject loginObjects = new JsonObject();
            loginObjects.addProperty("engaged_code", prefs.getString("engaged_code", "N/A"));

            Ion.with(this)
                    .load("POST", MED_SCHEDULE_URL)
                    .setLogging("BioData Logs", Log.DEBUG)
                    .setJsonObjectBody(loginObjects)
                    .asJsonArray()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                MedSchedule medSchedule;

                                String jsonString = result.toString();
//                                JSONObject jsonObject = new JSONObject(jsonString);
                                Log.d("Results", jsonString);
                                JSONArray scheduleArrayJSON = new JSONArray(jsonString);

                                //                              if (jsonObject.getString("resp_code").equals("000")) {


                                for (int i = 0; i < scheduleArrayJSON.length(); i++) {
                                    Random rnd = new Random();
                                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


                                    JSONObject scheduleObject = scheduleArrayJSON.getJSONObject(i);
                                    String f_name = scheduleObject.getString("facility_name");
                                    String sch_date = scheduleObject.getString("schedule_date");
                                    String freq = scheduleObject.getString("frequency");
                                    String ad_code = scheduleObject.getString("assigned_code");
                                    String sch_code = scheduleObject.getString("schedule_code");
                                    medSchedule = new MedSchedule();

                                    medSchedule.setAssigned_code("Assigned Code: " + ad_code);
                                    medSchedule.setFacility_name(f_name);
                                    medSchedule.setFrequency("Frequency: " + freq);
                                    medSchedule.setSchedule_code("Schedule Code: " + sch_code);
                                    medSchedule.setSchehule_date("Schedule Date: " + sch_date);

                                    medSchedule.setSide_color(color);

                                    medScheduleList.add(medSchedule);
                                }

                                medScheduleAdapter = new MedScheduleAdapter(medScheduleList, MedScheduleActivity.this);
                                recyclerSchedule.setAdapter(medScheduleAdapter);

                                //progressDialog.dismiss();
                                //finishActivity();
//                                } else {
//                                    //progressDialog.dismiss();
//                                    //displayDialog(getResources().getString(R.string.invalid_username_password));
//                                }
//

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                //progressDialog.dismiss();
                                //displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            //progressDialog.dismiss();
                            //displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            //progressDialog.dismiss();
            //displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }
}
