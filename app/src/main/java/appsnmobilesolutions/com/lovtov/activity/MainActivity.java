package appsnmobilesolutions.com.lovtov.activity;

import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ss.bottomnavigation.BottomNavigation;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.fragments.BioDataFragment;
import appsnmobilesolutions.com.lovtov.fragments.HomeFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnFragmentInteractionListener{
    Typeface typeface;
    ImageView logOut, profilePic;
    TextView welcomeAddress, location, employeeID, company, role, notifications, newsLetter;
    BottomNavigation bottomNavigation;
    LinearLayout notificationLinear, newsLetterLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerComponents();

   /*     notificationLinear.setOnClickListener(v -> {

        });


        newsLetterLinear.setOnClickListener(v -> {
        });

        logOut.setOnClickListener(v -> logOut());*/

        bottomNavigation.setDefaultItem(0);
        bottomNavigation.setOnSelectedItemChangeListener(itemId -> {
            FragmentTransaction transaction = null;


            switch (itemId) {
                case R.id.tab_home:
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, new HomeFragment());
                    break;
                case R.id.tab_reports:
                    //transaction = getSupportFragmentManager().beginTransaction();
                    //transaction.replace(R.id.content, new AddFundsFragment());
                    break;
                case R.id.tab_biodata:
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, new BioDataFragment());
                    break;
                case R.id.tab_pending_updates:
                    //transaction = getSupportFragmentManager().beginTransaction();
                    //transaction.replace(R.id.content, new RequestLoanFragment());
                    break;
            }

            if (transaction != null) {
                transaction.commit();
            }

        });

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        // Create a new fragment and specify the planet to show based on position
        Fragment fragment = null;

      /*  if (id == R.id.nav_home) {
            // Handle the settings action
            fragment = new HomeFragment();
        } else if (id == R.id.nav_add_funds) {
            fragment = new AddFundsFragment();
        } else if (id == R.id.nav_transfer_funds) {
            fragment = new TransferFundsFragment();
            //return  false;
        } else if (id == R.id.nav_logout) {
            //fragment = new TransferFundsFragment();
            logout();
            return false;
        } else {
            //fragment = new HomeFragment();
            return false;
        }*/

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void registerComponents() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
           actionBar.show();
        }

        //DrawerLayout drawer = findViewById(R.id.drawer_layout);
        //final ImageView btnOpenDrawer =  drawer.findViewById(R.id.drawer_icon);
        //btnOpenDrawer.setOnClickListener(v -> drawer.openDrawer(Gravity.START));
        //ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);
        //toggle.syncState();
       /* typeface = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        logOut = findViewById(R.id.log_out_button);
        welcomeAddress = findViewById(R.id.employee_name);
        location = findViewById(R.id.employee_location);
        employeeID = findViewById(R.id.employee_id);
        company = findViewById(R.id.employee_company);
        role = findViewById(R.id.employee_role);
        notifications = findViewById(R.id.notifications);
        newsLetter = findViewById(R.id.news_letter);
        notificationLinear = findViewById(R.id.notification_linear);
        newsLetterLinear = findViewById(R.id.newsletter_linear);*/
        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setDefaultItem(0);
        bottomNavigation.setTypeface(typeface);
        bottomNavigation.clearFocus();
       /* welcomeAddress.setTypeface(typeface);
        location.setTypeface(typeface);
        employeeID.setTypeface(typeface);
        company.setTypeface(typeface);
        role.setTypeface(typeface);
        notifications.setTypeface(typeface);
        newsLetter.setTypeface(typeface);*/
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
