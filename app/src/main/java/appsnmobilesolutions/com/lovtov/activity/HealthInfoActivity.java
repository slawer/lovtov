package appsnmobilesolutions.com.lovtov.activity;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import appsnmobilesolutions.com.lovtov.fragments.ConditionsFragment;
import appsnmobilesolutions.com.lovtov.fragments.AllergyFragment;
import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.adapter.HealthInfoAdapter;
import appsnmobilesolutions.com.lovtov.model.HealthInfo;
import appsnmobilesolutions.com.lovtov.util.ConnectionDetector;

import static appsnmobilesolutions.com.lovtov.util.APILinks.MED_INFO_URL;

public class HealthInfoActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private static List<HealthInfo> healthInfoList;

    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    ConnectionDetector connectionDetector;

    HealthInfoAdapter healthInfoAdapter;

    RecyclerView recyclerHealthInfo;

    BottomNavigationView navigation;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_info);

       registerComponents();
    }

    public void registerComponents(){
        connectionDetector = new ConnectionDetector(this);
        healthInfoList = new ArrayList<>();

        navigation = (BottomNavigationView) findViewById(R.id.navigation);

        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Health Info");
        toolbar.setTitleTextColor(getResources().getColor(R.color.text_color));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        prefs = PreferenceManager.getDefaultSharedPreferences(HealthInfoActivity.this);
        edit = prefs.edit();

        navigation.setOnNavigationItemSelectedListener(this);
        setDefaultFragment();
    }

    public void getMedicalInformation() {
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isNetworkAvailable()) {

            JsonObject loginObjects = new JsonObject();
            loginObjects.addProperty("engaged_code", prefs.getString("engaged_code", "N/A"));

            Ion.with(this)
                    .load("POST", MED_INFO_URL)
                    .setLogging("Med Logs", Log.DEBUG)
                    .setJsonObjectBody(loginObjects)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                HealthInfo allergy;
                                //HealthCondition healthCondition;
                                String jsonString = result.toString();
                                JSONObject jsonObject = new JSONObject(jsonString);
                                Log.d("Results", jsonString);

                                if (jsonObject.getString("resp_code").equals("000")) {
                                    JSONArray allergyArrayJSON = new JSONArray(jsonObject.getString("allergies"));
                                    JSONArray conditionArrayJSON = new JSONArray(jsonObject.getString("health_conditions"));

                                    for (int i = 0; i < allergyArrayJSON.length(); i++) {
                                        JSONObject allergyObject = allergyArrayJSON.getJSONObject(i);
                                        String userAllergy = allergyObject.getString("allergy");
                                        String recordDate = allergyObject.getString("record_date");
                                        allergy = new HealthInfo();
                                        allergy.setAllergyName("Allergy: " + userAllergy);
                                        allergy.setDateRecordedAllergy("Recorded On: " + recordDate);
                                        healthInfoList.add(allergy);
                                    }


                                    for (int i = 0; i < conditionArrayJSON.length(); i++) {
                                        JSONObject healthObject = conditionArrayJSON.getJSONObject(i);
                                        String healthConditionName = healthObject.getString("condition");
                                        String dateRecorded = healthObject.getString("record_date");
                                        allergy = new HealthInfo();
                                        allergy.setConditionName("Condition: " + healthConditionName);
                                        allergy.setDateRecordedCondition("Recorded On" + dateRecorded);
                                        healthInfoList.add(allergy);
                                    }


                                    healthInfoAdapter = new HealthInfoAdapter(healthInfoList, getApplicationContext());
                                    //allergyAdapter = new AllergyAdapter(allergylist, getApplicationContext());

                                    recyclerHealthInfo.setAdapter(healthInfoAdapter);
                                   // healthRecyclerView.setAdapter(healthConditionAdapter);


                                    //progressDialog.dismiss();
                                    //finishActivity();
                                } else {
                                    //progressDialog.dismiss();
                                    //displayDialog(getResources().getString(R.string.invalid_username_password));
                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                                //progressDialog.dismiss();
                                //displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            //progressDialog.dismiss();
                            //displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            //progressDialog.dismiss();
            //displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    private void setDefaultFragment() {
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, AllergyFragment.newInstance());
        transaction.commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selectedFragment=null;
        switch (item.getItemId()) {
            case R.id.nav_allergies:
                selectedFragment= AllergyFragment.newInstance();
                break;
            case R.id.nav_condition:
                selectedFragment= ConditionsFragment.newInstance();
                break;

        }

        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout,selectedFragment);
        transaction.commit();
        return true;
    }
}
