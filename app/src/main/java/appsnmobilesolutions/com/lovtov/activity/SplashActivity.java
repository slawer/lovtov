package appsnmobilesolutions.com.lovtov.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import appsnmobilesolutions.com.lovtov.R;

public class SplashActivity extends AppCompatActivity {

    TextView lovTov, description;
    Typeface typeface;
    static int SPLASH_TIME_OUT = 3000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        lovTov = findViewById(R.id.lov_top_text);
        description = findViewById(R.id.lov_top_description);
        typeface = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        lovTov.setTypeface(typeface);
        description.setTypeface(typeface);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);



    }
}
