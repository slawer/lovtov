package appsnmobilesolutions.com.lovtov.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.List;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.model.Escalation;

public class EscalationAdapter extends RecyclerView.Adapter<EscalationAdapter.EscalationItemHolder> {

    private List<Escalation> escalationList;
    private Context context;
    Typeface typeface;


    public EscalationAdapter(List<Escalation> escalationList, Context context) {
        super();
        this.escalationList = escalationList;
        this.context = context;
        notifyDataSetChanged();
        typeface = Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf");
    }

    @NonNull
    @Override
    public EscalationAdapter.EscalationItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.escalation_card_view, parent, false);
        EscalationAdapter.EscalationItemHolder viewHolder = new EscalationAdapter.EscalationItemHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull EscalationAdapter.EscalationItemHolder holder, int position) {


        Escalation escalation = escalationList.get(position);

        String image_file_name = escalation.getImage_path();
        holder.escalationDesc.setText(escalation.getEscalation());
        holder.approver_id.setText(escalation.getApprover_id());

        holder.updated_at.setText(escalation.getUpdated_at());

        if (escalation.getApproval_status().equals("null") || escalation.getApproval_status().equals("false")) {
            holder.approval_status.setTextColor(context.getResources().getColor(R.color.text_red));
            holder.approval_status.setText("pending");
        } else if (escalation.getApproval_status().equals("true")) {
            holder.approval_status.setTextColor(context.getResources().getColor(R.color.text_greeen));
            holder.approval_status.setText("approved");
        }


        holder.viewImage.setOnClickListener(v -> {
            showPhoto(image_file_name + "");
        });


        holder.side_color.setBackgroundColor(escalation.getSide_color());

        holder.escalationDesc.setTypeface(typeface);
        holder.approver_id.setTypeface(typeface);
        holder.approval_status.setTypeface(typeface);
        holder.updated_at.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return escalationList.size();
    }

    class EscalationItemHolder extends RecyclerView.ViewHolder {
        TextView escalationDesc, approver_id, approval_status, updated_at, side_color;
        Button viewImage;

        private EscalationItemHolder(View itemView) {
            super(itemView);
            escalationDesc = itemView.findViewById(R.id.complaint_desc);
            approver_id = itemView.findViewById(R.id.approved_id);
            approval_status = itemView.findViewById(R.id.approval_status);
            updated_at = itemView.findViewById(R.id.updated_at);
            viewImage = itemView.findViewById(R.id.viewImage);
            side_color = itemView.findViewById(R.id.side_color);
        }
    }


    private void showPhoto(String photoUri) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.escalation_image_holder);
        dialog.setCancelable(true);

        ImageView imgView = dialog.findViewById(R.id.imageEscalation);

        Ion.with(context)
                .load(photoUri)
                .intoImageView(imgView);

        dialog.show();
    }

}
