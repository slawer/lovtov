package appsnmobilesolutions.com.lovtov.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.model.Allergy;
import appsnmobilesolutions.com.lovtov.model.MedSchedule;

public class MedScheduleAdapter extends RecyclerView.Adapter<MedScheduleAdapter.MedScheduleItemHolder> {
    private List<MedSchedule> medScheduleList;
    private Context context;
    Typeface typeface;


    public MedScheduleAdapter(List<MedSchedule> medScheduleList, Context context) {
        super();
        this.medScheduleList = medScheduleList;
        this.context = context;
        typeface = Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf");
    }


    @Override
    public MedScheduleAdapter.MedScheduleItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.med_schedule_cardview, parent, false);
        MedScheduleAdapter.MedScheduleItemHolder viewHolder = new MedScheduleAdapter.MedScheduleItemHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MedScheduleAdapter.MedScheduleItemHolder holder, int position) {
        MedSchedule medSchedule = medScheduleList.get(position);
        holder.assigned_code.setText(medSchedule.getAssigned_code());
        holder.facility_name.setText(medSchedule.getFacility_name());
        holder.frequency.setText(medSchedule.getFrequency());
        holder.schedule_code.setText(medSchedule.getSchedule_code());
        holder.schedule_date.setText(medSchedule.getSchehule_date());

        holder.side_color.setBackgroundColor(medSchedule.getSide_color());


        holder.assigned_code.setTypeface(typeface);
        holder.facility_name.setTypeface(typeface);
        holder.frequency.setTypeface(typeface);
        holder.schedule_code.setTypeface(typeface);
        holder.schedule_date.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return medScheduleList.size();
    }

    class MedScheduleItemHolder extends RecyclerView.ViewHolder {
        TextView facility_name,schedule_date,frequency,assigned_code,schedule_code,side_color;

        private MedScheduleItemHolder(View itemView) {
            super(itemView);
            facility_name = itemView.findViewById(R.id.facility_name);
            schedule_date = itemView.findViewById(R.id.schedule_date);
            frequency = itemView.findViewById(R.id.frequency);
            assigned_code = itemView.findViewById(R.id.assigned_code);
            schedule_code = itemView.findViewById(R.id.schedule_code);
            side_color = itemView.findViewById(R.id.side_color);
        }
    }
}


