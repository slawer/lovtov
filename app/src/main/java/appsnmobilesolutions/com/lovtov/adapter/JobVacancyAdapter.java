package appsnmobilesolutions.com.lovtov.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import appsnmobilesolutions.com.lovtov.viewholder.JobChildViewHolder;
import appsnmobilesolutions.com.lovtov.viewholder.JobViewHolder;
import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.model.JobVacancyInfo;

public class JobVacancyAdapter extends ExpandableRecyclerViewAdapter<JobViewHolder,JobChildViewHolder> {

    public JobVacancyAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public JobViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.job_vacancy_cardview,parent,false);
        return new JobViewHolder(view);
    }

    @Override
    public JobChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.job_vacancy_child_cardview,parent,false);
        return new JobChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(JobChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        JobVacancyInfo jobVacancyInfo=(JobVacancyInfo) group.getItems().get(childIndex);

        holder.setJobDesc(jobVacancyInfo.getJobDesc());
        holder.setSideColor(jobVacancyInfo.getConColor());
    }

    @Override
    public void onBindGroupViewHolder(JobViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setJobTitle(group.getTitle());
    }
}
