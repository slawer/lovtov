package appsnmobilesolutions.com.lovtov.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.model.HealthCondition;

public class HealthConditionAdapter extends RecyclerView.Adapter<HealthConditionAdapter.HealthConditionItemHolder> {
    private List<HealthCondition> healthConditionList;
    private Context context;
    Typeface typeface;


    public HealthConditionAdapter(List <HealthCondition> healthConditionList, Context context) {
        super();
        this.healthConditionList = healthConditionList;
        this.context = context;
        typeface = Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf");
    }

    @NonNull
    @Override
    public HealthConditionItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.health_condition_cardview, parent, false);
        HealthConditionAdapter.HealthConditionItemHolder viewHolder = new HealthConditionAdapter.HealthConditionItemHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HealthConditionItemHolder holder, int position) {
        HealthCondition healthCondition = healthConditionList.get(position);
        holder.healthCondition.setText(healthCondition.getConditionName());
        holder.recordedDate.setText(healthCondition.getDateRecorded());
        holder.side_color.setBackgroundColor(healthCondition.getConColor());
        holder.healthCondition.setTypeface(typeface);
        holder.recordedDate.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return healthConditionList.size();
    }

    class HealthConditionItemHolder extends RecyclerView.ViewHolder {
        TextView healthCondition, recordedDate,side_color;

        private HealthConditionItemHolder(View itemView) {
            super(itemView);
            healthCondition = itemView.findViewById(R.id.condition_name);
            recordedDate = itemView.findViewById(R.id.date_recorded_condition);
            side_color = itemView.findViewById(R.id.side_color);
        }
    }
}

