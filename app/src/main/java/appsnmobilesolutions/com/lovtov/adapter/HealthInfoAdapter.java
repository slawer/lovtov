package appsnmobilesolutions.com.lovtov.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.model.HealthInfo;

public class HealthInfoAdapter extends RecyclerView.Adapter<HealthInfoAdapter.HealthInfoItemHolder> {

    private List<HealthInfo> healthInfoList;
    private Context context;
    Typeface typeface;


    public HealthInfoAdapter(List <HealthInfo> healthInfoList, Context context) {
        super();
        this.healthInfoList = healthInfoList;
        this.context = context;
        typeface = Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf");
    }

    @NonNull
    @Override
    public HealthInfoAdapter.HealthInfoItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.health_condition_cardview, parent, false);
        HealthInfoAdapter.HealthInfoItemHolder viewHolder = new HealthInfoAdapter.HealthInfoItemHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HealthInfoAdapter.HealthInfoItemHolder holder, int position) {
        HealthInfo healthCondition = healthInfoList.get(position);
        holder.healthCondition.setText(healthCondition.getConditionName());
        holder.recordedDateCondition.setText(healthCondition.getDateRecordedCondition());
        holder.allergyName.setText(healthCondition.getAllergyName());
        holder.recordedDateAllergy.setText(healthCondition.getDateRecordedAllergy());

        holder.healthCondition.setTypeface(typeface);
        holder.recordedDateCondition.setTypeface(typeface);
        holder.allergyName.setTypeface(typeface);
        holder.recordedDateAllergy.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return healthInfoList.size();
    }

    class HealthInfoItemHolder extends RecyclerView.ViewHolder {
        TextView healthCondition, recordedDateCondition,allergyName,recordedDateAllergy;

        private HealthInfoItemHolder(View itemView) {
            super(itemView);
            healthCondition = itemView.findViewById(R.id.condition_name);
            recordedDateCondition = itemView.findViewById(R.id.date_recorded_condition);
            allergyName = itemView.findViewById(R.id.allergy_name);
            recordedDateAllergy = itemView.findViewById(R.id.date_recorded_allergy);
        }
    }
}
