package appsnmobilesolutions.com.lovtov.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.model.Allergy;

public class AllergyAdapter extends RecyclerView.Adapter<AllergyAdapter.AllergyItemHolder> {
    private List<Allergy> allergyList;
    private Context context;
    Typeface typeface;


    public AllergyAdapter(List<Allergy> allergyList, Context context) {
        super();
        this.allergyList = allergyList;
        this.context = context;
        typeface = Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf");
    }


    @Override
    public AllergyItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.health_allergy_cardview, parent, false);
        AllergyAdapter.AllergyItemHolder viewHolder = new AllergyAdapter.AllergyItemHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AllergyItemHolder holder, int position) {
        Allergy allergy = allergyList.get(position);
        holder.allergyName.setText(allergy.getAllergyName());
        holder.recordedDate.setText(allergy.getDateRecorded());
        holder.side_color.setBackgroundColor(allergy.getAlColor());
        holder.allergyName.setTypeface(typeface);
        holder.recordedDate.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return allergyList.size();
    }

    class AllergyItemHolder extends RecyclerView.ViewHolder {
        TextView allergyName, recordedDate,side_color;

        private AllergyItemHolder(View itemView) {
            super(itemView);
            allergyName = itemView.findViewById(R.id.allergy_name);
            recordedDate = itemView.findViewById(R.id.date_recorded_allergy);
            side_color = itemView.findViewById(R.id.side_color);
        }
    }
}
