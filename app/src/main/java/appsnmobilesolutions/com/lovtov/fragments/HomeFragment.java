package appsnmobilesolutions.com.lovtov.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.marcoscg.dialogsheet.DialogSheet;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.activity.MedicalInformation;
import appsnmobilesolutions.com.lovtov.util.CustomViewDialog;
import dmax.dialog.SpotsDialog;


public class HomeFragment extends Fragment {
    LinearLayout notificationLinear, newsLetterLinear, accountLinear, medicalLinear;
    ImageView logOut, profilePic;
    TextView welcomeAddress, location, employeeID, company, role, notifications, newsLetter;
    Typeface typeface;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    ImageView drawerImageView;
    SpotsDialog progressDialog;

    public HomeFragment() {
    }


    @SuppressLint("CommitPrefEdits")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        edit = prefs.edit();

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular.ttf");
        logOut = rootView.findViewById(R.id.log_out_button);
        welcomeAddress = rootView.findViewById(R.id.employee_name);
        employeeID = rootView.findViewById(R.id.employee_id);
        role = rootView.findViewById(R.id.employee_role);
        notifications = rootView.findViewById(R.id.notifications);
        newsLetter = rootView.findViewById(R.id.news_letter);
        notificationLinear = rootView.findViewById(R.id.notification_linear);
        newsLetterLinear = rootView.findViewById(R.id.newsletter_linear);
        accountLinear = rootView.findViewById(R.id.account_linear);
        medicalLinear = rootView.findViewById(R.id.medical_linear);
        welcomeAddress.setTypeface(typeface);
        employeeID.setTypeface(typeface);
        role.setTypeface(typeface);
        notifications.setTypeface(typeface);
        newsLetter.setTypeface(typeface);
        welcomeAddress.setText("Welcome, " + prefs.getString("employee_first_name", "N/A"));
        employeeID.setText(prefs.getString("engaged_code", "N/A"));

        progressDialog = new SpotsDialog(getActivity(), R.style.Custom);
        progressDialog.setCancelable(true);

        //medicalLinear.setOnClickListener(v -> startActivity(new Intent(getActivity(), MedicalInformation.class)));
       medicalLinear.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               progressDialog.show();
               startActivity(new Intent(getActivity(), MedicalInformation.class));
               progressDialog.dismiss();
           }
       });
        accountLinear.setOnClickListener(v -> showGeneralInfo());
        logOut.setOnClickListener(v -> logOut());


        return rootView;
    }


    public void logOut() {
        CustomViewDialog alert = new CustomViewDialog();
        String message = "Continue Logout?";
        alert.showDialog(getActivity(), message);
    }


    public void showGeneralInfo() {
        /* new DialogSheet(getActivity())
                .setTitle("General Account Information")
                .setMessage("Test General Info\tName: Jude Botchwey\tEmployee Code: 10045")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogSheet.OnPositiveClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Your action
                    }
                })
                .setBackgroundColor(Color.WHITE) //Your custom background color
                .setButtonsColorRes(R.color.colorPrimary)  //Default color is accent
                .show();
        */

        DialogSheet dialogSheet = new DialogSheet(getActivity());
        dialogSheet.setView(R.layout.account_custom);
        View inflatedView = dialogSheet.getInflatedView();
        Button button = inflatedView.findViewById(R.id.okay_button);
        TextView nameTextView = inflatedView.findViewById(R.id.my_full_name);
        TextView ageTextView = inflatedView.findViewById(R.id.my_age);
        TextView employeeCode = inflatedView.findViewById(R.id.my_emp_code);
        nameTextView.setText(prefs.getString("employee_first_name", "N/A") + " " + prefs.getString("employee_last_name", "N/A"));
        ageTextView.setText(prefs.getString("age", "N/A") + " Years");
        employeeCode.setText(prefs.getString("engaged_code", "N/A"));
        button.setOnClickListener(v -> dialogSheet.dismiss());
        dialogSheet.show();
    }
}
