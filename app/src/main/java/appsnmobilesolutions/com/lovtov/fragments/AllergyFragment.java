package appsnmobilesolutions.com.lovtov.fragments;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import appsnmobilesolutions.com.lovtov.R;
import appsnmobilesolutions.com.lovtov.adapter.AllergyAdapter;
import appsnmobilesolutions.com.lovtov.adapter.HealthInfoAdapter;
import appsnmobilesolutions.com.lovtov.model.Allergy;
import appsnmobilesolutions.com.lovtov.model.HealthCondition;
import appsnmobilesolutions.com.lovtov.model.HealthInfo;
import appsnmobilesolutions.com.lovtov.util.ConnectionDetector;

import static appsnmobilesolutions.com.lovtov.util.APILinks.MED_INFO_URL;


public class AllergyFragment extends Fragment {

    View myFragement;
    private static List<Allergy> allergyList;

    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    ConnectionDetector connectionDetector;

    AllergyAdapter allergyAdapter;

    RecyclerView recyclerHealthInfo;


    public AllergyFragment() {
    }

    public static AllergyFragment newInstance() {
        AllergyFragment fragment = new AllergyFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragement=inflater.inflate(R.layout.fragment_allergy, container, false);

        connectionDetector = new ConnectionDetector(getActivity());
        allergyList = new ArrayList<>();

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        edit = prefs.edit();

        recyclerHealthInfo=myFragement.findViewById(R.id.recyclerAllergy);
        recyclerHealthInfo.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerHealthInfo.setLayoutManager(llm);

        getMedicalInformation();
        return myFragement;
    }

    public void getMedicalInformation() {
        connectionDetector = new ConnectionDetector(getActivity());
        if (connectionDetector.isNetworkAvailable()) {

            JsonObject loginObjects = new JsonObject();
            loginObjects.addProperty("engaged_code", prefs.getString("engaged_code", "N/A"));

            Ion.with(this)
                    .load("POST", MED_INFO_URL)
                    .setLogging("Med Logs", Log.DEBUG)
                    .setJsonObjectBody(loginObjects)
                    .asJsonObject()
                    .setCallback((e, result) -> {

                        if (result != null) {
                            try {

                                Allergy allergy;
                                //HealthCondition healthCondition;
                                String jsonString = result.toString();
                                JSONObject jsonObject = new JSONObject(jsonString);
                                Log.d("Results", jsonString);


                                if (jsonObject.getString("resp_code").equals("000")) {
                                    JSONArray allergyArrayJSON = new JSONArray(jsonObject.getString("allergies"));

                                    for (int i = 0; i < allergyArrayJSON.length(); i++) {
                                        Random rnd = new Random();
                                        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


                                        JSONObject allergyObject = allergyArrayJSON.getJSONObject(i);
                                        String userAllergy = allergyObject.getString("allergy");
                                        String recordDate = allergyObject.getString("record_date");
                                        allergy = new Allergy();
                                        allergy.setAllergyName(userAllergy);
                                        allergy.setDateRecorded(recordDate);
                                        allergy.setAlColor(color);
                                        allergyList.add(allergy);
                                    }

                                    allergyAdapter = new AllergyAdapter(allergyList, getActivity());
                                    recyclerHealthInfo.setAdapter(allergyAdapter);

                                    //progressDialog.dismiss();
                                    //finishActivity();
                                } else {
                                    //progressDialog.dismiss();
                                    //displayDialog(getResources().getString(R.string.invalid_username_password));
                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                                //progressDialog.dismiss();
                                //displayDialog(getResources().getString(R.string.unknown_error));
                            }
                        } else {
                            //progressDialog.dismiss();
                            //displayDialog(getResources().getString(R.string.unknown_error));
                            Log.e("Ion Exception", "wtf", e);
                        }
                    });
        } else {

            //progressDialog.dismiss();
            //displayDialog(getResources().getString(R.string.network_unavailable));

            //Toast.makeText(SignInActivity.this, "Network unavailable. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }
}
