package appsnmobilesolutions.com.lovtov.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import appsnmobilesolutions.com.lovtov.R;

/**
 * Created by Jude on 2/16/2018.
 */

public class BioDataFragment extends Fragment {

    public BioDataFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_bio_data, container, false);
        return rootView;
    }


}
