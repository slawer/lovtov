package appsnmobilesolutions.com.lovtov.model;

public class MedSchedule {
    String facility_name;
    String schedule_code;
    String schehule_date;
    String frequency;
    String assigned_code;
    int side_color;

    public String getFacility_name() {
        return facility_name;
    }

    public void setFacility_name(String facility_name) {
        this.facility_name = facility_name;
    }

    public String getSchedule_code() {
        return schedule_code;
    }

    public void setSchedule_code(String schedule_code) {
        this.schedule_code = schedule_code;
    }

    public String getSchehule_date() {
        return schehule_date;
    }

    public void setSchehule_date(String schehule_date) {
        this.schehule_date = schehule_date;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getAssigned_code() {
        return assigned_code;
    }

    public void setAssigned_code(String assigned_code) {
        this.assigned_code = assigned_code;
    }

    public int getSide_color() {
        return side_color;
    }

    public void setSide_color(int side_color) {
        this.side_color = side_color;
    }
}
