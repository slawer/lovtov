package appsnmobilesolutions.com.lovtov.model;

public class HealthInfo {
    String conditionName, allergyName, dateRecordedCondition,dateRecordedAllergy;

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public String getAllergyName() {
        return allergyName;
    }

    public void setAllergyName(String allergyName) {
        this.allergyName = allergyName;
    }

    public String getDateRecordedCondition() {
        return dateRecordedCondition;
    }

    public void setDateRecordedCondition(String dateRecordedCondition) {
        this.dateRecordedCondition = dateRecordedCondition;
    }

    public String getDateRecordedAllergy() {
        return dateRecordedAllergy;
    }

    public void setDateRecordedAllergy(String dateRecordedAllergy) {
        this.dateRecordedAllergy = dateRecordedAllergy;
    }
}
