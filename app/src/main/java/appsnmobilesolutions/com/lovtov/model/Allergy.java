package appsnmobilesolutions.com.lovtov.model;

public class Allergy {
    String allergyName, dateRecorded;
    int alColor;

    public String getAllergyName() {
        return allergyName;
    }

    public void setAllergyName(String allergyName) {
        this.allergyName = allergyName;
    }

    public String getDateRecorded() {
        return dateRecorded;
    }

    public void setDateRecorded(String dateRecorded) {
        this.dateRecorded = dateRecorded;
    }

    public int getAlColor() {
        return alColor;
    }

    public void setAlColor(int alColor) {
        this.alColor = alColor;
    }
}
