package appsnmobilesolutions.com.lovtov.model;

public class HealthCondition {

    String conditionName, dateRecorded;
    int conColor;

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public String getDateRecorded() {
        return dateRecorded;
    }

    public void setDateRecorded(String dateRecorded) {
        this.dateRecorded = dateRecorded;
    }

    public int getConColor() {
        return conColor;
    }

    public void setConColor(int conColor) {
        this.conColor = conColor;
    }
}
