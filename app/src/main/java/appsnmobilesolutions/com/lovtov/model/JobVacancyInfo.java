package appsnmobilesolutions.com.lovtov.model;

public class JobVacancyInfo {
    String jobTitle, jobDesc;
    String jobLocation, jobCatName;
    int conColor;

    public JobVacancyInfo(String jobDesc, int conColor) {
        this.jobDesc = jobDesc;
        this.conColor = conColor;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobDesc() {
        return jobDesc;
    }

    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getJobCatName() {
        return jobCatName;
    }

    public void setJobCatName(String jobCatName) {
        this.jobCatName = jobCatName;
    }

    public int getConColor() {
        return conColor;
    }

    public void setConColor(int conColor) {
        this.conColor = conColor;
    }
}
