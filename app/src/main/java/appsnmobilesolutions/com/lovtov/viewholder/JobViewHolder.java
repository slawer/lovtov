package appsnmobilesolutions.com.lovtov.viewholder;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import appsnmobilesolutions.com.lovtov.R;

public class JobViewHolder extends GroupViewHolder {

    private TextView jobTitle;
    private TextView side_color;

    public JobViewHolder(View itemView) {
        super(itemView);

        jobTitle = itemView.findViewById(R.id.job_title);
        side_color = itemView.findViewById(R.id.side_color);
    }

     public void setJobTitle(String name){
        jobTitle.setText(name);
     }

    public void setSideColor(int color) {
        side_color.setBackgroundColor(color);
    }
}
