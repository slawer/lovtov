package appsnmobilesolutions.com.lovtov.viewholder;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import appsnmobilesolutions.com.lovtov.R;

public class JobChildViewHolder extends ChildViewHolder{

    private TextView jobDesc;
    private TextView side_color;

    public JobChildViewHolder(View itemView) {
        super(itemView);

        jobDesc=itemView.findViewById(R.id.job_desc);
        side_color=itemView.findViewById(R.id.side_color);
    }

    public void setJobDesc(String name) {
        jobDesc.setText(name);
    }

    public void setSideColor(int color) {
        side_color.setBackgroundColor(color);
    }
}
