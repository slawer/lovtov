package appsnmobilesolutions.com.lovtov.util;



public interface APILinks {
     String  LOGIN_URL = "https://5.153.40.138:6660/api/lovtov/mobile/access_account";
     String SIGN_UP_URL = "https://5.153.40.138:6660/api/lovtov/mobile/create_account";
     String OFFSITE_SIGN_UP_URL = "https://5.153.40.138:6660/api/lovtov/mobile/create_offsite_account";
     String USER_TYPE_URL = "https://5.153.40.138:6660/api/lovtov/mobile/user_types";
     String MED_INFO_URL = "https://5.153.40.138:6660/api/lovtov/mobile/user_medical_info";
     String JOB_VACANCY_URL = "https://5.153.40.138:6660/api/lovtov/mobile/get_job_vacancies";
     String BIO_DATA_URL = "https://5.153.40.138:6660/api/lovtov/mobile/user_bio_data";
     String POST_BIO_DATA_URL = "https://5.153.40.138:6660/api/lovtov/mobile/post_user_bio_data";
     String MED_SCHEDULE_URL = "https://5.153.40.138:6660/api/lovtov/mobile/user_medical_schedule";
     String SEND_ESCALATION_URL = "https://5.153.40.138:6660/api/lovtov/mobile/upload_user_complaint";
     String ESCALATION_URL = "https://5.153.40.138:6660/api/lovtov/mobile/load_user_complaints";
}


